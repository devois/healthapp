import Expo, { Notifications } from 'expo';

import React from 'react';
import { StyleSheet, Text, View, Alert } from 'react-native';
import { Drawer, ActionConst, Scene, Reducer, Router, Switch, TabBar, Modal, Schema, Actions } from 'react-native-router-flux';

import WelcomeScreen from './screens/initialScreens/welcomeScreen';
import PhoneVerification from './screens/initialScreens/PhoneVerification/PhoneVerification';
import Countries from './screens/Countries';
import SignUpScreen1 from './screens/initialScreens/SignUpScreens/SignUpScreen1';
import SignUpScreen2 from './screens/initialScreens/SignUpScreens/SignUpScreen2';
import SignUpScreen3 from './screens/initialScreens/SignUpScreens/SignUpScreen3';
import SignUpScreen4 from './screens/initialScreens/SignUpScreens/SignUpScreen4';
import UploadCertificates from './screens/initialScreens/SignUpScreens/UploadCertificates';
import SignUpScreen6Upload from './screens/initialScreens/SignUpScreens/SignUpScreen6Upload';

import Preferences from './screens/SetPreferences/Preferences';
import CallAvailability from './screens/SetPreferences/CallAvailability';


export default class RouterClass extends React.Component {

  render() {    
    return (      
          <Router>
            <Scene key="root" hideNavBar={true} >
              <Scene key="welcome" component={WelcomeScreen} hideNavBar={true} />
              <Scene key="phoneverification" component={PhoneVerification} title="Verify" hideNavBar={true}/>
              <Scene key="countries" component={Countries} title="Countries"/> 
              <Scene key="signup1" component={SignUpScreen1} title="signup1" />
              <Scene key="signup2" component={SignUpScreen2} title="signup2" />
              <Scene key="signup3" component={SignUpScreen3} title="signup3" />
              <Scene key="signup4" component={SignUpScreen4} title="signup4" />
              <Scene key="uploadcertificates" component={UploadCertificates} title="uploadcertificates" />
              <Scene key="sigupupload6" component={SignUpScreen6Upload} title="sigupupload6" />

              <Scene key="preferences" component={Preferences} title="preferences" />
              <Scene key="callavailability" component={CallAvailability} title="callavailability" />
           </Scene>              
          </Router>
        
    );
  }
}

