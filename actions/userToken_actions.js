//storing user_token

import {
  USER_TOKEN,
  CLEAR_USER_ID
} from './types';

export const storeUser_id = (token) => {
  
  return {
    payload: token,
    type: USER_TOKEN
  };
};



export const clearUser_Id = () => {
  return { type: CLEAR_USER_ID };
};



