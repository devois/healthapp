import { combineReducers } from 'redux';
import userToken from './userToken_reducer';

export default combineReducers({
  userToken
});
