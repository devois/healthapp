import {
  USER_TOKEN,
  CLEAR_USER_ID
} from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case USER_TOKEN:
      return action.payload
    default:
      return state;
  }
}