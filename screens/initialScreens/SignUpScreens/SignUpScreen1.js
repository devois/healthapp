import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView
} from 'react-native';

import { LinearGradient } from 'expo'
import { colors, fontSizes, gradients } from '../../../constants/styles'
import Dropdown from '../../../components/Dropdown'
import TextField from '../../../components/TextField'
import Button from '../../../components/Button'

import RangeSlider from '../../../components/RangeSlider'
import ProgressWizard from '../../../components/ProgressWizard'

import { Actions } from 'react-native-router-flux';


export default class SignUpScreen1 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      gender: ''
    }
    this.handleChangeValue = this.handleChangeValue.bind(this);
    this._submitNext = this._submitNext.bind(this);
  }

  static navigationOptions = {
    //TODO
  };

  _submitNext(){
    console.log("SignUpScreen1");
    console.log(this.state.first_name, this.state.last_name, this.state.email, this.state.gender);
    let signupData = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      gender: this.state.gender
    }
    Actions.signup2({Data: signupData});
  }

  handleChangeValue(field_name, textInput){
    console.log(field_name, textInput);
    let textValue = textInput;
    let field_value = field_name;
    if( field_value == "first_name"){
      this.setState({first_name: textValue});
    }
    else if( field_value == "last_name"){
      this.setState({last_name: textValue});
    }
    else if( field_value == "email"){
      this.setState({email: textValue});
    }
     
  }

  render() {
    var gender = [
      {title: 'Male'},
      {title: 'Female'}
    ]
    
    return (
      <View style={styles['container']}>
        
        <LinearGradient colors={gradients['grayWhite']} style={styles['topCard']}>
          <Text style={styles['title']}>Create a new account</Text>
          <Text style={{color: colors.text, width: 200}}>Create an account with new phone number </Text>
          <View style={{marginTop: 32, flexDirection: 'row', justifyContent: 'center'}}>
            <ProgressWizard step={2}/>
          </View>
        </LinearGradient>
        <View style={styles['body']}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between', flex: 1}}>
              <Text style={styles['label']}>FIRST NAME</Text>
              <Text style={styles['label']}>LAST NAME</Text>
          </View>
        
          <View style={{flexDirection: 'row', justifyContent: 'space-between', flex: 1}}>
            <TextField
              placeholder="First name"
              style={{flex: 1, marginRight: 8}}
              prop_name="first_name"
              value={this.state.first_name}
              onChange={this.handleChangeValue}
            />
            <TextField
              placeholder="Last name"
              style={{flex: 1, marginRight: 8}}
              prop_name="last_name"
              value={this.state.last_name}
              onChange={this.handleChangeValue}
            />
          </View>
          <View style={styles['formItem']}>
            <Text style={styles['label']}>EMAIL</Text>
            <TextField 
              placeholder='Enter your Email'
              prop_name="email"
              value={this.state.email}
              onChange={this.handleChangeValue}
            />
          </View>
           <View style={styles['formItem']}>
            <Text style={styles['label']}>GENDER</Text>
            <Dropdown title={'Gender'} list={gender} onSelect={(obj,i) => this.setState({gender: obj.title})}></Dropdown>
          </View>

        </View>
        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this._submitNext} >Continue</Button>
        </LinearGradient>
      </View>

    )
  }

}
//<Close style={{...StyleSheet.absoluteFillObject, top: 12, left: 12}}/>
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: colors.white
  },
  title: {
    fontSize: fontSizes['lg'],
    marginVertical: 4
  },
  subtitle: {
    color: colors.text,
    marginVertical: 4
  },
  headerCard: {
    flex: 1,
    paddingTop: 32,
    paddingHorizontal: 24
  },
  form: {
    flex: 5,
    paddingHorizontal: 24    
  },
  bottomCard: {
    flex: 0.8,
    paddingHorizontal: 24,
    paddingVertical: 20,
    justifyContent: 'center'
  },
  formItem: {
    marginVertical: 8
  },
  label: {
    paddingVertical: 4,
    color: colors.text
  },
  body:{
    height: 180,
    paddingTop: 40, //TODO
    flex: 4,
    paddingHorizontal: 32,
    backgroundColor: colors.white
  },
  nameItem:{
    marginVertical: 8,
    marginRight: 15
  },

})
