import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity
} from 'react-native';

import { LinearGradient } from 'expo'

import { colors, fontSizes, gradients } from '../../../constants/styles'
import LeftArrow from '../../../assets/images/left_arrow.png'

import Dropdown from '../../../components/Dropdown'
import TextField from '../../../components/TextField'
import Button from '../../../components/Button'

import RangeSlider from '../../../components/RangeSlider'
import ProgressWizard from '../../../components/ProgressWizard'

import { Actions } from 'react-native-router-flux';


export default class SignUpScreen2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      specialization: '',
      bloodGroup: '',
      res_address: '',
      prevsignupData: {
        last_name: props.Data.last_name,
        first_name: props.Data.first_name,
        email: props.Data.email,
        gender: props.Data.gender
      }
    }
   //console.log(props.Data);
   // console.log(props.email);

    this.handleChangeValue = this.handleChangeValue.bind(this);
    this._submitNext = this._submitNext.bind(this);
  }

  static navigationOptions = {
    //TODO
  };

  _submitNext(){
    console.log("signup3");
    let signupData = this.state.prevsignupData;
    signupData.specialization = this.state.specialization;
    signupData.bloodGroup = this.state.bloodGroup;
    signupData.res_address = this.state.res_address;

    console.log('signupData', signupData);
    Actions.signup3({Data: signupData});
  }

  handleChangeValue(field_name, textInput){
    console.log(field_name, textInput);
    let textValue = textInput;
    let field_value = field_name;
    if( field_value == "res_address"){
      this.setState({res_address: textValue});
    }     
  }

  render() {
    var bloodGroups = [
      {title: 'A+ve'},
      {title: 'A-ve'},
      {title: 'B+ve'},
      {title: 'B-ve'},
      {title: 'AB+ve'},
      {title: 'AB-ve'},
      {title: 'O+ve'},
      {title: 'O-ve'}
    ]

    return (
      <View style={styles['container']}>
        
        <LinearGradient colors={gradients['grayWhite']} style={styles['topCard']}>
          <Text style={styles['title']}>Create a new account</Text>
          <Text style={{color: colors.text, width: 200}}>Fill up the following fields to finish signup </Text>
          <View style={{marginTop: 32, flexDirection: 'row', justifyContent: 'center'}}>
            <ProgressWizard step={2}/>
          </View>
        </LinearGradient>
        <TouchableOpacity style={styles.back} onPress={() => {}}>
          <Image source={LeftArrow} style={{marginRight: 8}}/>
          <Text style={{color: colors.blue, fontSize: fontSizes['sm']}}>BACK</Text>
        </TouchableOpacity>
        <View style={styles['body']}>
          <View style={styles['form']}>
          <View style={styles['formItem']}>
            <Text style={styles['label']}>BLOOD GROUP</Text>
            <Dropdown title={'Bloodgroup'} list={bloodGroups} onSelect={(obj,i) => this.setState({bloodGroup: obj.title})}></Dropdown>
          </View>
          <View style={styles['formItem']}>
            <Text style={styles['label']}>RESIDENTIAL ADDRESS</Text>
             <TextField 
              placeholder='Enter your address'
              prop_name="res_address"
              value={this.state.address}
              onChange={this.handleChangeValue}
            />
          </View>
          <View style={styles['formItem']}>
            <Text style={styles['label']}>SPECIALIZATION</Text>
            <Dropdown title={'Specialization'} list={bloodGroups} onSelect={(obj,i) => this.setState({specialization: obj.title})}></Dropdown>
          </View>
        </View>

        </View>
        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this._submitNext} >Continue</Button>
        </LinearGradient>
      </View>

    )
  }

}
//<Close style={{...StyleSheet.absoluteFillObject, top: 12, left: 12}}/>
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: colors.white
  },
  title: {
    fontSize: fontSizes['lg'],
    marginVertical: 4
  },
  subtitle: {
    color: colors.text,
    marginVertical: 4
  },
  headerCard: {
    flex: 1,
    paddingTop: 32,
    paddingHorizontal: 24
  },
  form: {
    flex: 5,
    paddingHorizontal: 24    
  },
  bottomCard: {
    flex: 0.8,
    paddingHorizontal: 24,
    paddingVertical: 20,
    justifyContent: 'center'
  },
  formItem: {
    marginVertical: 8
  },
  label: {
    paddingVertical: 4,
    color: colors.text
  },
  body:{
    height: 180,
    paddingTop: 40, //TODO
    flex: 4,
    paddingHorizontal: 32,
    backgroundColor: colors.white
  },
  nameItem:{
    marginVertical: 8,
    marginRight: 15
  },
  back: {
    flexDirection: 'row',
    marginTop: 45,
    marginLeft: 60
  }

})

