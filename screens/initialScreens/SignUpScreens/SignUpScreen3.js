import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image
} from 'react-native';

import { LinearGradient } from 'expo'
import { colors, fontSizes, gradients } from '../../../constants/styles'
import Dropdown from '../../../components/Dropdown'
import TextField from '../../../components/TextField'
import Button from '../../../components/Button'
import MultiSlider from '@ptomasroos/react-native-multi-slider';

import { debounce } from 'lodash'
import RangeSlider from '../../../components/RangeSlider'
import ProgressWizard from '../../../components/ProgressWizard'
import LeftArrow from '../../../assets/images/left_arrow.png'

import { Actions } from 'react-native-router-flux';


export default class SignUpScreen3 extends Component {
  constructor(props) {
    super(props);
     this.state = {
      experience : [5],
      registration_number: '',
      company_name: '',
      preffered_language: '',
      prevsignupData: {
        last_name: props.Data.last_name,
        first_name: props.Data.first_name,
        email: props.Data.email,
        gender: props.Data.gender,
        bloodGroup: props.Data.bloodGroup,
        res_address: props.Data.res_address,
        specialization: props.Data.specialization
      }
    }
    console.log(props);
    console.log('props', props.signupData);

    this.handleChangeValue = this.handleChangeValue.bind(this);
    this._submitNext = this._submitNext.bind(this);
  }

  static navigationOptions = {
    //TODO
  };

  
  multiSliderValuesChange = (values) => {
    this.setState({experience: values}, () => {
      // this.props.onChange(values); TODO
    });
  }

   _submitNext(){
    console.log("signup3");
    let signupData = this.state.prevsignupData;
    signupData.experience = this.state.experience;
    signupData.registration_number = this.state.registration_number;
    signupData.company_name = this.state.company_name;
    signupData.preffered_language = this.state.preffered_language;

    console.log('signupData', signupData);
    Actions.signup4(signupData);
  }

  handleChangeValue(field_name, textInput){
    console.log(field_name, textInput);
    let textValue = textInput;
    let field_value = field_name;
    if( field_value == "registration_number"){
      this.setState({registration_number
        : textValue});
    }
    else if(field_value == 'company_name'){
      this.setState({company_name: textValue});
    }
  }

  
  render() {
    var languages = [
      {title: 'English'},
      {title: 'Armenian'},
      {title: 'Japaneese'},
      {title: 'Chinese'},
      {title: 'Hindi'},
      {title: 'Arabic'},
      {title: 'Sanskrit'},
      {title: 'Mongolian'}
    ];
    let start = 8, stop = 18;

    return (
      <View style={styles['container']}>
        
        <LinearGradient colors={gradients['grayWhite']} style={styles['topCard']}>
          <Text style={styles['title']}>Create a new account</Text>
          <Text style={{color: colors.text, width: 200}}>Create an account with new phone number </Text>
          <View style={{marginTop: 32, flexDirection: 'row', justifyContent: 'center'}}>
            <ProgressWizard step={2}/>
          </View>
        </LinearGradient>

        <TouchableOpacity style={styles.back} onPress={() => {}}>
          <Image source={LeftArrow} style={{marginRight: 8}}/>
          <Text style={{color: colors.blue, fontSize: fontSizes['sm']}}>BACK</Text>
        </TouchableOpacity>

        <View style={styles['body']}>
          <ScrollView style={styles['form']}>
          <View style={styles['formItem']}>
            <Text style={styles['label']}>REGISTRATION NUMBER</Text>
            <TextField
              placeholder="Enter your registration number"
              prop_name="registration_number"
              value={this.state.registration_number}
              onChange={this.handleChangeValue}
            />
          </View>
          <View style={styles['formItem']}>
            <Text style={styles['label']}>COMPANY NAME</Text>
             <TextField
              placeholder="Enter your company name"
              prop_name="company_name"
              value={this.state.company_name}
              onChange={this.handleChangeValue}
            />
          </View>
          <View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 16}}>
              <Text>Experience</Text>
              <Text>{this.state.experience} years</Text>
            </View>
            <MultiSlider
              selectedStyle={{backgroundColor: colors.blue}}
              unselectedStyle={{backgroundColor: colors.lightGray}}
              markerStyle={{backgroundColor: colors.blue, height: 20, width: 20}}
              pressedMarkerStyle={{backgroundColor: colors.blue}}
              trackStyle={{height: 5, borderRadius: 6}}
              markerOffsetY={2}
              values={this.state.experience}
              sliderLength={320}
              onValuesChange={this.multiSliderValuesChange}
              min={1}
              max={30}
              step={1}
              snapped/>
          </View>
          <View style={styles['formItem']}>
            <Text style={styles['label']}>PREFFERED LANGUAGE</Text>
            <Dropdown title={'preffered langauage'} list={languages} onSelect={(obj,i) => this.setState({preffered_language: obj.title})}>></Dropdown>
          </View>
          
        </ScrollView>

        </View>
        <LinearGradient
         style={styles['bottomCard']}
         colors={gradients.whiteGray}>
          <Button size='lg' onPress={this._submitNext} >Continue</Button>
        </LinearGradient>
      </View>

    )
  }

}
//<Close style={{...StyleSheet.absoluteFillObject, top: 12, left: 12}}/>
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: colors.white
  },
  title: {
    fontSize: fontSizes['lg'],
    marginVertical: 4
  },
  subtitle: {
    color: colors.text,
    marginVertical: 4
  },
  headerCard: {
    flex: 1,
    paddingTop: 32,
    paddingHorizontal: 24
  },
  form: {
    flex: 5,
    paddingHorizontal: 24    
  },
  bottomCard: {
    flex: 0.8,
    paddingHorizontal: 24,
    paddingVertical: 20,
    justifyContent: 'center'
  },
  formItem: {
    marginVertical: 8
  },
  label: {
    paddingVertical: 4,
    color: colors.text
  },
  body:{
    height: 180,
    paddingTop: 20, //TODO
    flex: 4,
    paddingHorizontal: 32,
    backgroundColor: colors.white
  },
  nameItem:{
    marginVertical: 8,
    marginRight: 15
  },
  back: {
    flexDirection: 'row',
    marginTop: 45,
    marginLeft: 60
  }

})

