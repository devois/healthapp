import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import {
  Button
} from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import { LinearGradient } from 'expo';

import { colors, gradients, fontSizes } from '../../constants/styles'




class WelcomeScreen extends React.Component {
 
  _onSubmit(){
    Actions.phoneverification();
  }

  render() {  
    return (
      <LinearGradient style={styles.container} colors={gradients.greenBlue}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'flex-start'}}>
          <Text style={styles.text}> Welcome  To Health App</Text>
           <Button
              onPress={this._onSubmit.bind(this)}
              title="Next"
              large
              
            />
        </View>       
      </LinearGradient>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    alignItems: 'center'
  },
  text: {
    fontSize: fontSizes['x-lg'],
    color: colors.white,
    textAlign: 'center'
  }
});

export default WelcomeScreen;